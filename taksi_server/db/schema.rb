# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151215090817) do

  create_table "bookings", force: :cascade do |t|
    t.string   "contact"
    t.string   "status"
    t.float    "fee"
    t.string   "location"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "driver_id"
    t.string   "destination"
    t.boolean  "paid"
  end

  add_index "bookings", ["driver_id"], name: "index_bookings_on_driver_id"

  create_table "drivers", force: :cascade do |t|
    t.string   "username"
    t.string   "password"
    t.string   "phoneNumber"
    t.string   "email"
    t.string   "status"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.datetime "lastResponded"
    t.string   "location"
  end

end
