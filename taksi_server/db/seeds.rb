# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


drivers = [{ :username => 'username1', :password => '1', :status => 'Invisible', :location => '{"lat" : 58.37,"lng" : 26.11}'},
           {:username => 'username2', :password => '1', :status => 'Off-duty', :location => '{"lat" : 58.37,"lng" : 26.11}'},
           {:username => 'username3', :password => '1', :status => 'Off-duty', :location => '{"lat" : 58.37,"lng" : 26.11}'},
           {:username => 'username4', :password => '1', :status => 'Off-duty', :location => '{"lat" : 58.37523,"lng" : 26.71}'},
           {:username => 'username5', :password => '1', :status => 'Off-duty', :location => '{"lat" : 58.374,"lng" : 26.74}'},
           {:username => 'username6', :password => '1', :status => 'Busy', :location => '{"lat" : 58.374,"lng" : 26.71}'},
           {:username => 'username7', :password => '1', :status => 'Available', :location => '{"lat" : 58.373,"lng" : 26.72}'},
           {:username => 'username8', :password => '1', :status => 'Available', :location => '{"lat" : 58.37711,"lng" : 26.72083}'},
           {:username => 'username9', :password => '1', :status => 'Available', :location => '{"lat" : 58.3777,"lng" : 26.72332}'},
           {:username => 'username10', :password => '1', :status => 'Available', :location => '{"lat" : 58.37123,"lng" : 26.72}'},
           {:username => 'username11', :password => '1', :status => 'Available', :location => '{"lat" : 58.37623,"lng" : 26.7243}'},
           {:username => 'username12', :password => '1', :status => 'Available', :location => '{"lat" : 58.37623,"lng" : 26.7233}'},
           {:username => 'username13', :password => '1', :status => 'Available', :location => '{"lat" : 58.37323,"lng" : 26.7263}'},
]


drivers.each do |driver|
  Driver.create!(driver)
end

bookings = [{:location => '{"lat" : 58.47,"lng" : 26.51}', :driver_id => 4, :status => 'accepted', :destination => 'Nooruse 7',:paid=>false},
            {:location => '{"lat" : 58.57,"lng" : 26.41}', :driver_id => 5, :status => 'finished', :destination => 'Nooruse 7',:paid=>false},
            {:location => '{"lat" : 58.67,"lng" : 26.31}', :driver_id => 6, :status => 'accepted', :destination => 'Nooruse 7',:paid=>false},
            {:location => '{"lat" : 58.77,"lng" : 26.21}', :driver_id => 7, :status => 'finished', :destination => 'Nooruse 7',:paid=>false},
            {:location => '{"lat" : 58.87,"lng" : 26.11}', :driver_id => 8, :status => 'accepted', :destination => 'Nooruse 7',:paid=>false},
            {:location => '{"lat" : 58.37763099999999,"lng" : 26.727978}', :driver_id => 6, :status => 'pending', :destination => 'Nooruse 7',:paid=>false},
]

bookings.each do |booking|
  Booking.create!(booking)
end