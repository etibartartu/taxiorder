class AddLocationToDrivers < ActiveRecord::Migration
  def change
    add_column :drivers, :location, :string
  end
end
