class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.string :contact
      t.string :status
      t.integer :fee
      t.string :location
      t.timestamps null: false
    end
  end
end
