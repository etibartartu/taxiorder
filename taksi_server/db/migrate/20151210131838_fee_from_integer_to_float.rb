class FeeFromIntegerToFloat < ActiveRecord::Migration
  def change
    change_column :bookings, :fee,  :float
  end
end
