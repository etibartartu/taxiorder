class AddDriverToBookings < ActiveRecord::Migration
  def change
    add_reference :bookings, :driver, index: true, foreign_key: true
  end
end
