class AddLastRespondedToDrivers < ActiveRecord::Migration
  def change
    add_column :drivers, :lastResponded, :datetime
  end
end
