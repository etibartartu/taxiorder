class CreateDrivers < ActiveRecord::Migration
  def change
    create_table :drivers do |t|
      t.string :username
      t.string :password
      t.string :phoneNumber
      t.string :email
      t.string :status
      t.timestamps :lastResponded
      t.timestamps null: false
    end
  end
end


