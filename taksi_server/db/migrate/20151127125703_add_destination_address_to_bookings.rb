class AddDestinationAddressToBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :destination, :string
  end
end
