Given(/^I am at (Liivi \d+)$/) do |arg1|
  uri = URI('http://maps.googleapis.com/maps/api/geocode/json?address=Liivi%202,%20sTartu,%20Estonia&sensor=false')
  response = JSON.parse(Net::HTTP.get(uri))["results"][0]["geometry"]["location"]
  (response["lat"].round 2).should be(58.38)
  (response["lng"].round 2).should be(26.71)
end

Given(/^there are two taxi available, one next to Kaubamaja and the other at Lounakeskus$/) do
  
end

When(/^I submit a booking request$/) do
  pending # express the regexp above with the code you wish you had
end

Then(/^I should receive a confirmation with the taxi next to Kaubamaja$/) do
  pending # express the regexp above with the code you wish you had
end

Then(/^I should receive a delay estimate$/) do
  pending # express the regexp above with the code you wish you had
end

Given(/^there are two taxi available, at the same distance with respect to Liivi$/) do
  pending # express the regexp above with the code you wish you had
end

Then(/^I should receive a confirmation with the taxi that has been available the longest$/) do
  pending # express the regexp above with the code you wish you had
end