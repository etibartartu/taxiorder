Feature: Taxi booking system
  As a customer
  So that I have a taxi booking
  I want to reject my booking request

  @javascript
  Scenario: Asynchronous callback
    Given I am in the booking page
    When I decline my booking
    And I fill in the reason field
    And I submit this information
    Then I should be notified that my booking has been canceled