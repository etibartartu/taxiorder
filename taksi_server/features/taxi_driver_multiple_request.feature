Feature: Taxi selection
  As a taxi driver
  So that I can have multiple customer request from STRS
  I need to confirm one of the request or not
  Background:
    Given I am at Kaubamaja
    And there are three request available at Liivi 1, Narva mnt 27, Raatuse 22
    When I check booking requests
  Scenario: Taxi driver accepts one of the requests
    When The taxi driver accepts the booking
    Then STRS checks the driver as occupied
    And STRS accepts the other requests as canceled
    And should be resent the other requests to the others taxi
  Scenario: Taxi driver clicks rejects all
    When The taxi driver rejects all the bookings
    Then STRS puts the driver to the end of queue
    And STRS  And should be resent the other requests to the others taxi
  Scenario: Request timeout
    When The taxi driver does not reply before 30 seconds
    Then STRS shouldn't reset the time taxi was unable
    And STRS should be resent to the other taxi