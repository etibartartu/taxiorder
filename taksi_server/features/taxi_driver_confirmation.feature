Feature: Taxi selection
  As a taxi driver
  So that I can have a customer request from STRS
  I need to confirm or not
  Background:
    Given I am at Kaubamaja
    And there are one request available at Liivi 1
    When I check booking requests
  Scenario: Taxi driver accepts the request
    When The taxi driver accepts the booking
    Then STRS checks the driver as occupied
  Scenario: Taxi driver rejects booking
    When The taxi driver rejects the booking
    Then STRS puts the driver the end of queue
  Scenario: Request timeout
    When The taxi driver does not reply before 30 seconds
    Then STRS shouldn't reset the time taxi was unable
    And STRS should be resent to the other taxi