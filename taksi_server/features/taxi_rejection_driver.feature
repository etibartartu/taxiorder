Feature: Taxi Rejection
  As a taxi driver
  So that I have a customer request from STRS
  I need to decline booking request
  Background:
    Given I  have a booking request
    When I check ignore  request
  Scenario: Taxi declines ignores the request
    When The taxi driver ignores the booking
    Then STRS should be resent to the other taxi
 