Feature: Login system
  As a Driver
  So that I can login
  if I am registered
  @javascript
  Scenario: Login
    Given I am in the /login page
    When I provide my login correct credentials
    And I click submit button
    Then I should be forwarded to home page
  Scenario: Wrong Login
    Given I am in the /login page
    When I provide my login wrong credentials
    And I click submit button
    Then I should be see error