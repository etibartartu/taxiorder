Feature: Boking timeout
  As a Driver
  if I don't responde to request
  then It should be forwarded another driver
  Background: movies in database

    Given the following driver exist:
      | username     | password |status|
      | username5    | 1        |busy  |
      | username6    | 1        |active|
    Given the following booking exist
      | location                         | driver_id |status  |
      | {"lat" : 58.47,"lng" : 26.51}    |  1        |pending |
  Background:
    Given There is a request to driver username5
  Scenario: The driver which has booking get push notifications
    When Driver gets notification about booking
    And Driver doesn't do anything
    Then the booking should be forwarded username6
    And Driver username6 should receive booking