Feature: Taxi status update
  As a taxi driver
  So that I do not receive any orders while I am off duty
  I need to set my status to 'off-duty'
  Background:
    Given I am at taxi parking base
    And my working hours are over
    When I set my status to 'off-duty'
  Scenario: Taxi driver working hours are over
    When The taxi driver sets status to 'off-duty'
    Then STRS does not forward any requests to this driver
    And TaxiHome will not report the location of the Taxi to STRS
  Scenario: Taxi driver waits for orders
    When The taxi driver sets status to 'available'
    Then STRS forwards available orders to the driver
    And TaxiHome will periodically report the location of the taxi to STRS
