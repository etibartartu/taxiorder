Feature: Taxi Invoice
  As a driver
  So that I can have a taxi booking finished
  I want my invoice to be shown my last 30 days invoice
  Given the following driver exist:
  | username     | password |status|
  | username5    | 1        |busy  |
  Given the following booking exist
  | location                         | driver_id |status  |
  | {"lat" : 58.47,"lng" : 26.51}    |  1        |active |
  Background:
    Given I am home page for driver
    And I have active booking
  Scenario: Invoice
    Given There is on going booking
    When I click finish to the active request
    And I click Invoice from the menu
    Then I should see the booking in my invoices