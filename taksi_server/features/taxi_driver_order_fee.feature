Feature: Taxi order fee
  In order to see a fee
  As a taxi driver
  I need to see via the TaxiHome app my account statement
  Background:
    Given I got paid for my drive
    And I finished my order
    Then STRS will calculate 3% fee 
  Scenario: Taxi driver want to see orders history
    When Taxi driver clicks account statement
    Then STRS calculates for current and previous months total fee
    And STRS shows driver the current and previous months history with total fee