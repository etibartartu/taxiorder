class Booking < ActiveRecord::Base
  belongs_to :drivers
  validates :location, presence:true
end
