class Driver < ActiveRecord::Base
  has_many :bookings
  validates :username,  presence: true, length: { maximum: 50 }
  validates :password,  presence: true
  
  def self.authenticate(username, password)
    driver = find_by_username(username)
    if driver && driver.password == password
      driver
    else
      nil
    end
  end
end
