class DriversController < ApplicationController
  def index
    booking=Booking.where(:driver_id=>params["driver"]["id"],:status=>'active').first

    if(booking)
      address=booking.location
      l=locationParse(address)
      location=Geocoder.address(l)
      render :json => {:haveBooking=> true,
                     :beginning => location,
                     :destination=>booking.destination,
                     :bookingid=>booking.id ,
                     :fee=>booking.fee
           }, :status => :created
    else
        render :json => {:haveBooking=> false, :message => "you don't have active booking"}, :status => :created
    end

  end

  def edit
    @driver = Driver.find params[:id]
  end
  #shows bookings of the driver
  def show
    booking=Booking.where(:driver_id=>params["driver"]["id"],:status=>'pending').first
    if(booking!=nil)
      address=booking.location
      l=locationParse(address)
      location=Geocoder.address(l)
      driver=Driver.find(params["driver"]["id"])
      driverLoc=locationParse(driver.location)
      render :json => {:haveBooking=> true,
                       :location => location,
                       :destination=>booking.destination,
                       :bookingid=>booking.id ,:driverid=>params["driver"]["id"],
                       :lat =>l[0], :lng=>l[1],
                       :myLat=>driverLoc[0],:myLng=>driverLoc[1],
                       :fee=>booking.fee
             }, :status => :created
    else
      render :json => {:haveBooking=> false, :message => "You don't have booking requests"}, :status => :created
    end

  end

  #detects answer to the booking from driver
  def answer
    message=''
    booking=Booking.find(params["bookingid"])
    if (params["answer"]=='finished')
      booking.status='finished'
      booking.save!
      driver=driverUpdate(booking.driver_id,"Available",DateTime.now)
      Pusher.trigger('bookings', 'async_notification', { delivered:true, bookingId:booking.id })
      message="Good Job. You succesfully delivered customer"
    elsif(params["answer"]=='ignored')
      driver=Driver.find(booking.driver_id)

      if(driver.status=='Busy' && booking.status=='pending')
        driver=driverUpdate(booking.driver_id,"Available",DateTime.now)
        #new driver search
        arg=locationParse(booking.location)
        BookingsAsyncJob.new.async.assignToDriver(booking, arg[0],arg[1])
        message="Cancelation is done"
      end
    elsif(params["answer"])

      booking.status="active"
      booking.save!
      message="Customer is waiting"
      Pusher.trigger('bookings', 'async_notification', { message: "#{params[:duration]}", bookingId:booking.id , pusherTrue:true})
    else
      driver=driverUpdate(booking.driver_id,"Available",DateTime.now)
      #new driver search
      arg=locationParse(booking.location)
      BookingsAsyncJob.new.async.assignToDriver(booking, arg[0],arg[1])
      message="Cancelation is done"
    end
    render :json => {:message => message}, :status => :created
  end

  def update
    @driver = Driver.find params[:id]
    @driver.update_attributes!(driver_params)
    redirect_to drivers_path
  end

  def driver_params
    params.require(:driver).permit(:username, :password, :status)
  end

  def login
    driver = Driver.authenticate(params[:username], params[:password])
    if driver
      session[:user_id] = driver.id
      render :json => {:message => "Logged in", :isLogin => true, :userId => session[:user_id]}
    else
      render :json => {:message => "Error", :isLogin => false, :user_id => session[:user_id]}
    end

  end

  def invoice
    bookings=Booking.where(:driver_id=>params["driver"]["id"],:status=>'finished',:created_at=> Date.today.beginning_of_month..Date.today.end_of_month,:paid=>false)
    #Booking.where(:driver_id=>params["driver"]["id"],:status=>'finished',:created_at=>30.days.ago..Time.now)
    total= ( Booking.where(:status=>'finished',:driver_id=>params["driver"]["id"],:created_at=> Date.today.beginning_of_month..Date.today.end_of_month,:paid=>false).sum :fee)
    yesPayment=false
    if(bookings.count>0)
      yesPayment=true
    end
    charged=(total*0.03).round(2)
    render :json => {:total => total, :bookings => bookings,:charged=>charged,:yesPayment=>yesPayment}
  end


  def invoicePay
    bookings=Booking.where(:driver_id=>params["driver"]["id"],:status=>'finished',:created_at=> Date.today.beginning_of_month..Date.today.end_of_month,:paid=>false).update_all(:paid=>true)
    render :json => {:paid => true, :driverId => params["driver"]["id"]}
  end

  def location
    #{"lat"=>58.3767894, "long"=>26.720647099999997, "driverId"=>"12", "controller"=>"drivers", "action"=>"location", "driver"=>{}}
    driver = Driver.find(params[:driverId]);
    driver.location="{\"lat\" : #{params[:lat]},\"lng\" : #{params[:long]}}";
    driver.save!
    render :json => {:message => "location updated"}
  end

  def requestStatus
    driver = Driver.find(params[:id]);
    render :json => {:message => 'Driver status request', :driverStatus => driver.status}
  end

  def updateStatus
    driver = Driver.find(params[:id])
    driver.status = params[:driverStatus]
    driver.save!
    render :json => {:message => 'Driver status successfully updated'}
  end

end
