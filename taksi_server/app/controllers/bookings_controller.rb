class BookingsController < ApplicationController
  def index
    @booking = Booking.all
  end

  def create
    booking = Booking.new(
        :location => "{'lat' : #{params[:latitude]},'lng' : #{params[:longitude]}}",
        :status => 'pending',
        :fee=> params[:fee],
        :destination => params[:destination],
        :paid=>false
    )
    booking.save!
    session[:client]=booking.id
    #    bookingId=assignToDriver(params[:latitude], params[:longitude])
    #BookingsAsyncJob.new.async.perform("Your booking has been confirmed", 0, 0)
    BookingsAsyncJob.new.async.assignToDriver(booking, params[:latitude], params[:longitude])
    #BookingsAsyncJob.new.async.perform("Your booking has been confirmed", 0, 0)
    render :json => {
           :message => "#{params[:fee]}",
           :bookingId=>booking.id
    }, :status => :created
  end

  def edit
    @booking = Booking.find params[:bookingId]
  end

  def show
    @booking = Booking.find params[:id]
  end

  def update
    @booking = Booking.find params[:bookingId]
    @booking.status="canceled"
    @booking.save!
    driverUpdate(@booking.driver_id,"Available",0)
    Pusher.trigger('drivers', 'async_notification', {:canceled=> true,:bookingid=>@booking.id ,:driverid=>@booking.driver_id})
    render :json => {
               :message => "Booking is canceled. ",
               :bookingId=>@booking.id
           }, :status => :created
  end

  def booking_params
    params.require(:booking).permit(:location, :driver_id, :status)
  end

end