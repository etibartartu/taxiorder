class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  skip_before_filter :verify_authenticity_token
  before_filter :cors_preflight

  def cors_preflight
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'GET,POST,PUT,PATCH,DELETE,OPTIONS'
    headers['Access-Control-Allow-Headers'] = 'Origin,Accept,Content-Type'
    headers['Access-Control-Max-Age'] = '3628800'
    head :ok, content_type: 'text/html' if request.request_method == 'OPTIONS'
  end

  def driverUpdate (id,status,time)
    driver=Driver.find(id)
    driver.lastResponded=time
    driver.status=status
    driver.save!
    return driver
  end

  def locationParse (x)
    arg=x.scan(/(\d+[.]\d+)/)
    return [arg[0][0],arg[1][0]]
  end
end