class BookingsAsyncJob
  include SuckerPunch::Job
  def assignToDriver(booking, latitude, longitude)
    Geocoder.configure(:timeout => 30)
    address=Geocoder.address([latitude,longitude])
    activedrivers=Driver.where(:status=>'Available')
    if (activedrivers.size > 0)
      distancesToDrivers=Hash.new
      activedrivers.each do |driver|
        arg=locationParse driver.location
        tempDistance=Geocoder::Calculations.distance_between([latitude,longitude], [arg[0],arg[1]])
        if(tempDistance<30)
          distancesToDrivers[driver.id]=driver.lastResponded.to_i
        end
      end
      closestDriver=distancesToDrivers.key(distancesToDrivers.values.min)
      booking.driver_id=closestDriver;
      booking.save!
      assignedDriver=Driver.find(closestDriver)
      assignedDriver.lastResponded=DateTime.now
      assignedDriver.status="Busy"
      assignedDriver.save!
      driverLoc=locationParse assignedDriver.location
      booking.save!
      Pusher.trigger('drivers', 'async_notification', {:haveBooking=> true, :location => address, :destination=>booking.destination, :bookingid=>booking.id ,:driverid=>closestDriver, :lat =>latitude, :lng=>longitude, :myLat=>driverLoc[0],:myLng=>driverLoc[1], :fee=>booking.fee})
    else
      # TODO should tell the client that there are no available driverstu
      booking.status='finished';
      booking.save!
      Pusher.trigger('bookings', 'async_notification', { noAvailableMessage: "No avaliable drivers ", noAvailable:true, bookingId:booking.id })
    end

  end

  def locationParse (x)
    arg=x.scan(/(\d+[.]\d+)/)
    return [arg[0][0],arg[1][0]]
  end
end

=begin
address=Geocoder.address([latitude,longitude])
activedrivers=Driver.where(:status=>'active')
distancesToDrivers=Hash.new
activedrivers.each do |driver|
  arg=locationParse driver.location
  tempDistance=Geocoder::Calculations.distance_between([latitude,longitude], [arg[0],arg[1]])
  if(tempDistance<10)
    distancesToDrivers[driver.id]=driver.lastResponded.to_i
  end
end
closestDriver=distancesToDrivers.key(distancesToDrivers.values.min)
message.driver_id=closestDriver;
message.save!
assignedDriver=Driver.find(closestDriver)
assignedDriver.lastResponded=DateTime.now
assignedDriver.status="busy"
assignedDriver.save!
booking.save!
Pusher.trigger('drivers', 'async_notification', { message: "you have request to  " +address })




  def bestPossibleDriver(bookingid,lat,log)
    activedrivers=Driver.where(:status=>'active')
    distancesToDrivers=Hash.new
    debugger;
    activedrivers.each do |driver|
      arg=locationParse driver.location
      tempDistance=Geocoder::Calculations.distance_between([lag,long], [arg[0],arg[1]])
      if(tempDistance<10)
        distancesToDrivers[driver.id]=driver.lastResponded.to_i
      end
    end
    closestDriver=distancesToDrivers.key(distancesToDrivers.values.min)
    driverNew=driverUpdate(closestDriver,"busy")
    booking=Booking.find(bookingid);
    booking.driver_id=driverNew.id
    booking.save!
    Pusher.trigger('bookings', 'async_notification', { message: "you have request to " + driverNew.id})
  end

=end
