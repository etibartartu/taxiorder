FactoryGirl.define do
  factory :booking do
    location     '{"lat" : 58.37,"lng" : 26.71}'
    driver_id     1
    status        "accepted"
    id            1
  end

  factory :bookingCreate, class: Booking do
    location     "{'lat' : 50,'lng' : 69}"
    driver_id     1
    fee           23
    status        "pending"
    id            2
  end

end
