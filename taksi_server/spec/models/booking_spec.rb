require 'rails_helper'

describe Booking do
	it "is invalid without a location" do
	   contact = Booking.new(location: nil)
	   contact.valid?
	   expect(contact.errors[:location]).to include("can't be blank")
	end
end