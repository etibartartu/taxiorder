require 'rails_helper'

describe Driver do
  it 'username should be present' do
    # expect(FactoryGirl.build(:driver, :username => nil).should_not be_valid)
   	contact = Driver.new(username: nil)
   	contact.valid?
   	expect(contact.errors[:username]).to include("can't be blank")
  end

  it 'password should be present' do
    contact = Driver.new(password: nil)
   	contact.valid?
   	expect(contact.errors[:password]).to include("can't be blank")
  end

  it 'should check password correct' do
    @fakeDriver = create(:driver)
    expect(Driver.authenticate("username1", "1")).to eq(@fakeDriver)
  end

  it 'should check password incorrect' do
  	@fakeDriver = create(:driver)
    expect(Driver.authenticate("asd", "12")).to be_nil
  end

end