require 'rails_helper'

describe DriversController do
	before :each do
	    allBooking = Booking.all
	    allBooking.each do |booking|
	      booking.delete
	    end

	    allDrivers = Driver.all
	    allDrivers.each do |driver|
	      driver.delete
	    end
	    @fakeDriver = create(:driver)
	end

	after(:all) do
	    # allDrivers = Driver.all
	    # allDrivers.each do |driver|
	    #   driver.delete
	    # end
	    
	    # allBooking = Booking.all
	    # allBooking.each do |booking|
	    #   booking.delete
	    # end
	end

	describe 'get driver`s active bookings' do
		it 'it should get all driver`s active bookings' do
	    	fakeBooking = create(:booking, status: "active")
	    	expect(Booking).to receive(:where).with({:driver_id => "1", :status => "active"}).and_return([fakeBooking])
	    	post :index, {:driver => {id: "1"}}
		end

		it 'it should get all error message when no booking selected' do
	    	expect(Booking).to receive(:where).with({:driver_id => "23", :status => "active"}).and_return([nil])
	    	post :index, {:driver => {id: "23"}}
		end
	end

	describe 'driver login' do
		it 'it should have correct login pass' do  
			expect(Driver).to receive(:authenticate).with("username1", "1").and_return(@fakeDriver)
			post :login, {
	          :username => "username1",
	          :password => "1"
	        }
			expect(response.body).to include({message: "Logged in", :isLogin => true, :userId => 1}.to_json)
		end

		it 'it should have wrong login pass' do  
			expect(Driver).to receive(:authenticate).with("asd", "12").and_return(nil)
			post :login, {
	          :username => "asd",
	          :password => "12"
	        }
			expect(response.body).to include({:message => "Error", :isLogin => false, :user_id => nil}.to_json)
		end
	end

	describe 'shows bookings of the driver' do
		it 'it should show bookings to the driver correct booking' do
			fakeBooking = create(:booking, status: "pending")
			expect(Booking).to receive(:where).with({:driver_id => "1", :status => "pending"}).and_return([fakeBooking])
			post :show, {:driver => {id: "1"}}
			expect(response.body).to include(
			{:haveBooking=> true, :location => "Lembitu 6-8, 50406 Tartu, Estonia",
                       :destination=>nil,
                       :bookingid=>1 ,:driverid=>"1",
                       :lat =>"58.37", :lng=>"26.71",
                       :myLat=>"58.371",:myLng=>"26.711",
                       :fee=>nil
             }.to_json)
		end

		it 'it should show bookings to the driver correct booking' do
			expect(Booking).to receive(:where).with({:driver_id => "1", :status => "pending"}).and_return([nil])
			post :show, {:driver => {id: "1"}}
			expect(response.body).to include({"haveBooking": false,"message": "You don't have booking requests"}.to_json)	
		end
	end

	describe 'shows bookings of the driver' do
		it 'it should show bookings to the driver correct booking' do
			fakeBooking = create(:booking)
			expect(Booking).to receive(:where).with({:driver_id => "1", :status => "pending"}).and_return([fakeBooking])
			post :show, {:driver => {id: "1"}}
			expect(response.body).to include({"haveBooking": true,"location": "Lembitu 6-8, 50406 Tartu, Estonia","destination": nil,"bookingid": 1,"driverid": "1","lat": "58.37","lng": "26.71","myLat": "58.371","myLng": "26.711","fee": nil}.to_json)
		end

		it 'it should show bookings to the driver correct booking' do
			fakeBooking = build(:booking)
			expect(Booking).to receive(:where).with({:driver_id => "1", :status => "pending"}).and_return([nil])
			post :show, {:driver => {id: "1"}}
			expect(response.body).to eq({"haveBooking": false,"message": "You don't have booking requests"}.to_json)	
		end
	end

	describe 'answer booking' do
		before :each do
			@fakeBooking = create(:booking, status: 'pending')
			expect(Booking).to receive(:find).with("1").and_return(@fakeBooking)
		end
		
		it 'it should answer to the driver in case of finished' do
			expect(Driver).to receive(:find).with(1).and_return(@fakeDriver)
			post :answer, {:bookingid => "1", answer: "finished"}
			expect(response.body).to eq({"message": "Good Job. You succesfully delivered customer"}.to_json)	
		end

		it 'it should answer to the driver in case of ignored' do
			fakeDriver = create(:driver, status: 'Busy')
			expect(Driver).to receive(:find).with(1).and_return(fakeDriver)
			expect(Driver).to receive(:find).with(1).and_return(fakeDriver)
			post :answer, {:bookingid => "1", answer: "ignored"}
			expect(response.body).to eq({"message": "Cancelation is done"}.to_json)	
		end

		# it 'it should answer to the driver in case of client waiting' do
		# 	expect { post :answer, {:bookingid => "1", :answer => true}}.to raise_exception
		# 	# expect(response.body).to eq({"message": "Customer is waiting"}.to_json)
		# end


		it 'it should answer to the driver in case of client cancel' do
			post :answer, {:bookingid => "1", :answer => false}
			expect(response.body).to eq({"message": "Cancelation is done"}.to_json)
		end
	end

	describe 'invoice' do
		it 'it should give invoice' do
			post :invoice, {:driver => {id: "1"}}
			expect(response.body).to eq({"total": 0.0,"bookings": [],"charged": 0.0}.to_json)
		end
	end

	describe 'location' do
		it 'it should show location updated message' do
			post :location, {:driverId => 1}
			expect(response.body).to eq({:message => "location updated"}.to_json)
		end
	end
end
