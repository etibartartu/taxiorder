require 'rails_helper'

describe BookingsController, :type => :controller do
  before :each do
    allBooking = Booking.all
    allBooking.each do |booking|
      booking.delete
    end

    @fakeDriver = build(:driver)
    @fakeBookingCreate = build(:bookingCreate)
  end

  after(:all) do
    allBooking = Booking.all
    allBooking.each do |booking|
      booking.delete
    end
  end

  # describe 'create booking' do
  #   it 'it should be able to create booking' do
  #     expect(Booking).to receive(:new).with(:location => "{'lat' : 50,'lng' : 69}",
  #                                           										          :status => 'pending',
  #                                           										          :fee=> "23",
  #                                           		          :destination => "address").and_return(@fakeBookingCreate)
  #     post :create, {
  #                     :latitude => "50",
  #                     :longitude => "69",
  #                     :fee => "23",
  #                     :destination => "address"
  #                 }
  #     expect(response.body).to eq({:message => "23", :bookingId=>2}.to_json)
  #   end
  # end

  describe 'cancel booking' do
    it 'it should be able to cancel booking' do
      @fakeBookingCreate = create(:bookingCreate)
      @fakeDriver = create(:driver)
      # expect(Booking).to receive(:find).with("2")
      # byebug
      post :update, {:bookingId => "2"}
      expect(response.body).to eq({:message => "Booking is canceled. ", :bookingId => 2}.to_json)

    end
  end

end