Rails.application.routes.draw do
  #With the settings above, we are stating that we will accept AJAX style requests from any device.
  match '*all' => 'application#cors_preflight', via: :options
  post "/bookings", :to => "bookings#create"
  post "/bookings/cancel", :to => "bookings#update"
  post "/drivers/request", :to=>"drivers#show"
  post "/drivers/answer", :to=>"drivers#answer"
  post "/drivers/accepted", :to=>"drivers#index"
  post "/drivers/requestStatus", :to=>"drivers#requestStatus"
  post "/drivers/updateStatus", :to=>"drivers#updateStatus"
  post "/drivers/location", :to=>"drivers#location"
  post "/drivers/invoice", :to=>"drivers#invoice"
  post "/drivers/invoicePayment", :to=>"drivers#invoicePay"
  post "/login", :to=>"drivers#login"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
