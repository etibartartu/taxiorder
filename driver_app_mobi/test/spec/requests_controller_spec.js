'use strict'

describe('RequestsCtrl', function(){
    beforeEach(module('driver_client'));

    var RequestsCtrl,
        RequestsService,
        RequestsAnswerService,
        scope,
        HEROKU_APP
        window

    beforeEach(inject(function($controller, $rootScope, _RequestsService_, _RequestsAnswerService_, _$window_, _HEROKU_APP_){
        scope = $rootScope.$new();
        window = _$window_;
        window.sessionStorage.userId = 7;
        HEROKU_APP = _HEROKU_APP_;
        RequestsService = _RequestsService_;
        RequestsAnswerService = _RequestsAnswerService_;
        RequestsCtrl = $controller('RequestsCtrl', {
            $scope: scope,
            $window: window
        });
    }));

    var $httpBackend = {};

    beforeEach(inject(function (_$httpBackend_) {
        $httpBackend = _$httpBackend_;
    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('should give info if you have request', function(){
        $httpBackend.expectPOST(HEROKU_APP + 'drivers/request', {id: '7'})
            .respond(201, {
                haveBooking: true,
                location: "Kure 5, 50405 Tartu, Estonia",
                destination: "Kastani 181a, 50410 Tartu, Estonia",
                bookingid: 16,
                driverid: "7",
                lat: "58.36080122281345",
                lng: "26.703118840844695",
                mylat: "58.373",
                myLng: "26.72",
                fee: 4
            });
        $httpBackend.flush();
        expect(scope.haveBooking).toEqual(true);
        expect(scope.bookingid).toEqual(16);
        expect(scope.fromLat).toEqual('58.36080122281345');
        expect(scope.fromLng).toEqual('26.703118840844695');
        expect(scope.destination).toEqual('Kastani 181a, 50410 Tartu, Estonia');
        expect(scope.fee).toEqual(4);

        $httpBackend.expectPOST(HEROKU_APP + 'drivers/answer', {
            answer: true,
            bookingid: 16,
            distance: 0,
            duration: 0,
        }).respond(201, {message: 'Customer is waiting'});
        scope.submit();
        $httpBackend.flush();
        expect(scope.syncNotification).toEqual('Customer is waiting');
        expect(scope.message).toEqual(true);
        expect(scope.haveBooking).toEqual(false);

        $httpBackend.expectPOST(HEROKU_APP + 'drivers/answer', {
            answer: false,
            bookingid: 16
        }).respond(201, {message: 'Cancelation is done'});
        scope.cancel();
        $httpBackend.flush();
        expect(scope.syncNotification).toEqual('Cancelation is done');
        expect(scope.message).toEqual(true);
        expect(scope.haveBooking).toEqual(false);
    });
});

describe('RequestsCtrl', function(){
    beforeEach(module('driver_client'));

    var RequestsCtrl,
        RequestsService,
        RequestsAnswerService,
        Framework,
        HEROKU_APP,
        scope,
        window

    beforeEach(inject(function($controller, $rootScope, _RequestsService_, _RequestsAnswerService_, _$window_, _HEROKU_APP_){
        scope = $rootScope.$new();
        window = _$window_;
        window.sessionStorage.userId = '1';
        HEROKU_APP = _HEROKU_APP_;
        RequestsService = _RequestsService_;
        RequestsAnswerService = _RequestsAnswerService_;
        RequestsCtrl = $controller('RequestsCtrl', {
            $scope: scope,
            $window: window,
        });
    }));

    var $httpBackend = {};

    beforeEach(inject(function (_$httpBackend_) {
        $httpBackend = _$httpBackend_;
    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('should not show bookings if i dont have any', function(){
        $httpBackend.expectPOST(HEROKU_APP + 'drivers/request', {id: '1'})
            .respond(201, {
                haveBooking: false,
                message: 'No bookings',
                bookingid: 0
            });
        $httpBackend.flush();
        expect(scope.haveBooking).toEqual(false);
        expect(scope.bookingid).toEqual(0);
        expect(scope.syncNotification).toEqual('No bookings');
    });
});