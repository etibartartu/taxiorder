'use strict';

describe('DriversCtrl', function(){
  beforeEach(module('driver_client'));

  var DriverLoginService,
      DriversService,
      DriversCtrl,
      HEROKU_APP,
      scope,
      window

  beforeEach(inject(function($controller, $rootScope, _$window_, _DriverLoginService_, _DriversService_, _HEROKU_APP_){
    DriverLoginService = _DriverLoginService_;
    DriversService = _DriversService_;
    scope = $rootScope.$new();
    HEROKU_APP = _HEROKU_APP_;
    window = _$window_;
    window.sessionStorage.token = true;
    window.sessionStorage.userId = 2;
    DriversCtrl = $controller('DriversCtrl', {
      $scope: scope,
      $window: window
    });
  }));

  var $httpBackend = {};

  beforeEach(inject(function (_$httpBackend_) {
    $httpBackend = _$httpBackend_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  beforeEach(function(){
    scope.requestMessage = '';
    scope.haveRequest = false;
    scope.requestId = '';
    scope.requestBeginning = '';
    scope.requestDestination = '';
    scope.requestFee = '';
    scope.requestCanceled = false;
  });

  it('should test pusher, booking view, and driversCtrl functions', function(){
    $httpBackend.expectPOST(HEROKU_APP + 'drivers/accepted', {id: '2'})
    .respond(201, {
      haveBooking: true,
      beginning: 'some address 1',
      destination: 'some address 2',
      bookingid: 21,
      fee: 5
    });

    $httpBackend.expectPOST(HEROKU_APP + 'drivers/request', {id: '2'})
    .respond(201, {
      haveBooking:true,
      location:"Nooruse 7a, 50411 Tartu, Estonia",
      destination:"Salme 16, 50104 Tartu, Estonia",
      bookingid:9,
      driverid:"2",
      lat:"58.3647181",
      lng:"26.6893001",
      myLat:"58.3647181",
      myLng:"26.6893001",
      fee:5.13
    });

    $httpBackend.flush();

    expect(scope.haveRequest).toEqual(true);
    expect(scope.requestId).toEqual(9);
    expect(scope.requestBeginning).toEqual('Nooruse 7a, 50411 Tartu, Estonia');
    expect(scope.requestDestination).toEqual('Salme 16, 50104 Tartu, Estonia');
    expect(scope.requestFee).toEqual(5.13);
    expect(scope.requestCanceled).toEqual(false);

    //Pusher part
    expect(scope.requestMessage).toBeUndefined;
    var stub = Pusher.instances[0];
    var channel = stub.channel('drivers');
    channel.emit('async_notification', {
      driverid:2,
      haveBooking:false,
      message: 'No bookings'
    });
    expect(scope.haveRequest).toEqual(false);
    expect(scope.requestMessage).toEqual('No bookings');

    //Finished
    scope.haveBooking = true;
    $httpBackend.expectPOST(HEROKU_APP + 'drivers/answer', {
      answer: 'finished',
      bookingid: 2
    }).respond(201, {
      message: 'some message'
    });
    scope.bookingId = 2
    scope.finished();
    $httpBackend.flush();
    expect(scope.syncNotification).toEqual('some message');
    expect(scope.haveBooking).toEqual(false);

    //Call at timeout
    scope.haveBooking = true;
    $httpBackend.expectPOST(HEROKU_APP + 'drivers/answer', {
      answer: 'ignored'
    }).respond(201, {
      message: 'some message'
    });
    scope.callAtTimeout();
    $httpBackend.flush();
    expect(scope.syncNotification).toEqual('some message');
    expect(scope.haveBooking).toEqual(true);
  });

  it('should not show bookings if you dont have bookings', function(){
    $httpBackend.expectPOST(HEROKU_APP + 'drivers/accepted', {id: '2'})
    .respond(201, {
      haveBooking: true,
      beginning: 'some address 1',
      destination: 'some address 2',
      bookingid: 21,
      fee: 5
    });

    $httpBackend.expectPOST(HEROKU_APP + 'drivers/request', {id: '2'})
    .respond(201, {
      haveBooking:false,
      message:"You don't have booking requests"
    });

    $httpBackend.flush();
    
    expect(scope.haveRequest).toEqual(false);
    expect(scope.requestCanceled).toEqual(false);
    expect(scope.requestMessage).toEqual("You don't have booking requests");
  });
});

describe('LoginCtrl', function(){
  beforeEach(module('driver_client'));

  var LoginCtrl,
      DriverLoginService,
      HEROKU_APP,
      scope,
      window

  beforeEach(inject(function($controller, $rootScope, _$window_, _DriverLoginService_, _HEROKU_APP_){
    scope = $rootScope.$new();
    window = _$window_;
    HEROKU_APP = _HEROKU_APP_;
    DriverLoginService = _DriverLoginService_;
    LoginCtrl = $controller('LoginCtrl', {
      $scope: scope,
      $window: window
    })
  }));

  var $httpBackend = {};

  beforeEach(inject(function (_$httpBackend_) {
    $httpBackend = _$httpBackend_;
  }));

  beforeEach(function(){
    window.sessionStorage.logged = false;
    window.sessionStorage.token = false;
    window.sessionStorage.userId = null;
    expect(scope.loginMessage).toBeUndefined();
  });

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should log in when correct username and password are submitted', function(){
    $httpBackend.expectPOST(HEROKU_APP + 'login', {username: 'username1', password: '1'})
        .respond(201, {message: 'Logged in', isLogin: true, userId: 13});
    scope.username = 'username1';
    scope.password = '1';
    scope.submit();
    $httpBackend.flush();
    expect(scope.loginMessage).toEqual('logedIn');
    expect(window.sessionStorage.logged).toEqual('true');
    expect(window.sessionStorage.token).toEqual('true');
    expect(window.sessionStorage.userId).toEqual('13');
  });

  it('should not log in if username/password incorrect', function(){
    $httpBackend.expectPOST(HEROKU_APP + 'login', {username: 'x', password: 'y'})
        .respond(500, {message: 'Error', isLogin: false, userId: null});
    scope.username = 'x';
    scope.password = 'y';
    scope.submit();
    $httpBackend.flush();
    expect(scope.loginMassage).toBeUndefined();
    expect(window.sessionStorage.logged).toEqual('false');
    expect(window.sessionStorage.token).toEqual('false');
    expect(window.sessionStorage.userId).toEqual('null');
  });
});

describe('Logout controller test', function(){
  beforeEach(module('driver_client'));

  var LogoutCtrl,
      scope,
      window

  beforeEach(inject(function($controller, $rootScope, _$window_){
    scope = $rootScope.$new();
    window = _$window_;
    window.sessionStorage.token = true;
    window.sessionStorage.userId = 'username1';
    window.sessionStorage.logged = true;
    scope.$parent.logged = true;
    LogoutCtrl = $controller('LogoutCtrl', {
      $scope: scope,
      $window: window,
    })
  }));

  //When Logout controller is reached it should set some session variables to false
  it('should logout if you are logged in', function(){
    expect(window.sessionStorage.token).toEqual('false');
    expect(window.sessionStorage.userId).toEqual('false');
    expect(window.sessionStorage.logged).toEqual('false');
    expect(scope.$parent.logged).toEqual(false);
  });
});

describe('InvoicesCtrl', function(){
  beforeEach(module('driver_client'));

  var InvoicesCtrl,
      InvoicesService,
      HEROKU_APP,
      scope,
      window

  beforeEach(inject(function($controller, $rootScope, _$window_, _InvoicesService_, _HEROKU_APP_){
    scope = $rootScope.$new();
    window = _$window_;
    window.sessionStorage.token = true;
    window.sessionStorage.userId = 2;
    HEROKU_APP = _HEROKU_APP_;
    InvoicesService = _InvoicesService_;
    InvoicesCtrl = $controller('InvoicesCtrl', {
      $scope: scope,
      $window: window
    })
  }));

  var $httpBackend = {};

  beforeEach(inject(function (_$httpBackend_) {
    $httpBackend = _$httpBackend_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should test invoices controller', function(){
    expect(scope.total).toEqual('');
    expect(scope.charged).toEqual('');
    expect(scope.bookings).toEqual('');
    $httpBackend.expectPOST(HEROKU_APP + 'drivers/invoice', {id: '2'}).respond(201, {
      total: 100,
      bookings: ['booking1', 'booking2'],
      charged: 200
    });
    $httpBackend.flush();
    expect(scope.total).toEqual(100);
    expect(scope.charged).toEqual(200);
    expect(scope.bookings).toEqual(['booking1', 'booking2']);
  });
});

describe('StatusCtrl', function(){
  beforeEach(module('driver_client'));

  var StatusCtrl,
      StatusRequestService,
      StatusUpdateService,
      HEROKU_APP,
      scope,
      window

  beforeEach(inject(function($controller, $rootScope, _$window_, _StatusRequestService_, _StatusUpdateService_, _HEROKU_APP_){
    scope = $rootScope.$new();
    window = _$window_;
    window.sessionStorage.token = true;
    window.sessionStorage.logged = true;
    window.sessionStorage.userId = 2;
    HEROKU_APP = _HEROKU_APP_;
    StatusRequestService = _StatusRequestService_;
    StatusUpdateService = _StatusUpdateService_;
    StatusCtrl = $controller('StatusCtrl', {
      $scope: scope,
      $window: window
    })
  }));

  var $httpBackend = {};

  beforeEach(inject(function (_$httpBackend_) {
    $httpBackend = _$httpBackend_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should test status controller', function(){
    expect(scope.asyncNotification).toEqual('');
    expect(scope.syncNotification).toEqual('');
    expect(scope.bookingid).toEqual('');
    expect(scope.driverStatus).toEqual('');
    expect(scope.disabled).toEqual(false);
    expect(scope.statusList).toEqual(['Available','Off-duty']);
    $httpBackend.expectPOST(HEROKU_APP + 'drivers/requestStatus', {id: '2'}).respond(201, {
      driverStatus: 'Busy',
    });
    $httpBackend.flush();
    expect(scope.disabled).toEqual(true);
    expect(scope.driverStatus).toEqual('Busy');

    scope.driverStatus = 'Off-duty'
    $httpBackend.expectPOST(HEROKU_APP + 'drivers/updateStatus',
      {id: '2', driverStatus: 'Available'}).respond(201);
    scope.update('Available');
    $httpBackend.flush();
    expect(scope.driverStatus).toEqual('Available');
  });
});
//   $scope.update = function(status) {
//     StatusUpdateService.save({id: $window.sessionStorage.userId, driverStatus: status}, function(response){
//       $window.location.reload();
//     });
//   };