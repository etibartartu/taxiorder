'use strict'

var app = angular.module('driver_client');

app.controller('RequestsCtrl', function ($scope,RequestsService,RequestsAnswerService,$window,Framework) {
    $scope.asyncNotification = '';
    $scope.syncNotification = '';
    $scope.haveBooking='';
    $scope.bookingid='';
    $scope.message='';
    $scope.$parent.logged = $window.sessionStorage.logged;
    $scope.fromLat="";
    $scope.fromLng= "";
    $scope.destination=0;
    $scope.myLat=0;
    $scope.myLot=0;
    $scope.fee=0.0;
    $scope.distance=0.0;
    $scope.duration=0.0;

    RequestsService.save({id: $window.sessionStorage.userId}, function (response) {
            $scope.message=true;
            $scope.syncNotification = response.message;
            $scope.haveBooking=response.haveBooking;
            $scope.bookingid=response.bookingid;
            $scope.fromLat=response.lat;
            $scope.fromLng= response.lng;
            $scope.destination=response.destination;
            $scope.myLat=response.myLat;
            $scope.myLog=response.myLng;
            $scope.fee=response.fee;
            $scope.caldistance(parseFloat($scope.myLat),parseFloat($scope.myLog),parseFloat($scope.fromLat),parseFloat($scope.fromLng));
    });

    $scope.submit = function() {
        RequestsAnswerService.save({answer: true, bookingid: $scope.bookingid , distance:$scope.distance, duration: $scope.duration}, function (response) {
            $scope.syncNotification = response.message;
            $scope.message=true;
            //console.log( response.message)
            $scope.haveBooking=false
        });
    };

    $scope.cancel = function() {
        RequestsAnswerService.save({answer: false, bookingid: $scope.bookingid}, function (response) {
            $scope.syncNotification = response.message;
            $scope.message=true
            //console.log( response.message)
            $scope.haveBooking=false
        });
    };

    var service = new google.maps.DistanceMatrixService
    $scope.caldistance= function(myLat,myLog,fromLat,fromLng)
    {

        service.getDistanceMatrix({
            origins: [{lat: myLat, lng: myLog}],
            destinations: [{lat: fromLat, lng: fromLng}],
            travelMode: google.maps.TravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false
        }, function (response, status) {
            if (status !== google.maps.DistanceMatrixStatus.OK) {
                alert('Error was: ' + status);
            } else {
                var originList = response.originAddresses;
                var destinationList = response.destinationAddresses;
                // console.log("burda");
                // console.log(destinationList);
                var element = response.rows[0].elements[0];
                var distance = element.distance.text;
                var duration = element.duration.text;
                // console.log(distance+"distance");
                // console.log(duration+" duration")
                $scope.$apply(function () {
                    $scope.distance = distance;
                    $scope.duration=duration;
                });


            }
        });
    }

});