'use strict'

var app = angular.module('driver_client');

app.controller('DriversCtrl', function ($scope, DriversService, PusherService, DriverLoginService,RequestsAnswerService,RequestsService, $window, $location, $timeout) {

  //common variables
  $scope.asyncNotification = '';
	$scope.syncNotification = '';

  //request variables
	$scope.haveBooking=false;
  $scope.bookingMessage='';
	$scope.bookingId='';
  $scope.bookingBeginning='';
  $scope.bookingDestination='';
  $scope.bookingFee=0;
	$scope.$parent.logged = $window.sessionStorage.logged;

  //active booking variables
  $scope.requestId='';
  $scope.requestMessage='';
  $scope.haveRequest=false;
  $scope.fromLng= "";
  $scope.requestBeginning='';
  $scope.requestDestination='';
  $scope.requestFee=0;
  $scope.requestDistance=0;
  $scope.requestDuration=0;
  $scope.requestCanceled=false;

	PusherService.onMessage(function(response) {

    if($window.sessionStorage.userId==response.driverid) {
      $scope.requestMessage = response.message;
      $scope.haveRequest=response.haveBooking;
      $scope.requestId=response.bookingid;
      $scope.requestBeginning=response.location;
      $scope.requestDestination=response.destination;
      $scope.requestFee=response.fee;
      if ($scope.haveRequest)
      {
        $scope.caldistance(parseFloat(response.myLat),parseFloat(response.myLng),parseFloat(response.lat),parseFloat(response.lng));
      }
    }
    $timeout(function () {
      //console.log("timeout")
      $scope.callAtTimeout(response.bookingid);
    }, 40000);
	});

	if ($window.sessionStorage.token == "true") {
	  DriversService.save({id: $window.sessionStorage.userId}, function (response) {
      //console.log(response.haveBooking)
		  $scope.haveBooking=response.haveBooking;
		  $scope.bookingMessage = response.message;
		  $scope.bookingBeginning=response.beginning;
		  $scope.bookingDestination=response.destination;
		  $scope.bookingId=response.bookingid;
		  $scope.bookingFee=response.fee;
	  });
  } else {
		$location.path('/login');
  }

	$scope.callAtTimeout=function(bookingId){
		//console.log("Timeout occurred");
		RequestsAnswerService.save({answer: 'ignored', bookingid: bookingId}, function (response) {
			$scope.syncNotification = response.message;
		});
	};

	$scope.finished = function() {
		RequestsAnswerService.save({answer: 'finished', bookingid: $scope.bookingId}, function (response) {
			$scope.syncNotification = response.message;
			$scope.haveBooking=false;
		});
	};

  RequestsService.save({id: $window.sessionStorage.userId}, function (response) {
    $scope.requestMessage = response.message;
    $scope.haveRequest=response.haveBooking;
    $scope.requestId=response.bookingid;
    $scope.requestBeginning=response.location;
    $scope.requestDestination=response.destination;
    $scope.requestFee=response.fee;
    if ($scope.haveRequest)
    {
      $scope.requestCanceled=false;
      $scope.caldistance(parseFloat(response.myLat),parseFloat(response.myLng),parseFloat(response.lat),parseFloat(response.lng));
    }
    if(response.canceled)
    {
      $scope.requestCanceled=true;
    }

  });

  $scope.answer = function() {
    RequestsAnswerService.save({answer: true, bookingid: $scope.requestId , distance:$scope.requestDistance, duration: $scope.requestDuration}, function (response) {
      $scope.syncNotification = response.message;
      $scope.haveRequest=false
      $window.location.reload();
    });
  };

  $scope.cancel = function() {
    RequestsAnswerService.save({answer: false, bookingid: $scope.requestId}, function (response) {
      $scope.syncNotification = response.message;
      $scope.haveRequest=false
    });
  };
  var service = new google.maps.DistanceMatrixService
  $scope.caldistance= function(myLat,myLog,fromLat,fromLng)
  {

    service.getDistanceMatrix({
      origins: [{lat: myLat, lng: myLog}],
      destinations: [{lat: fromLat, lng: fromLng}],
      travelMode: google.maps.TravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem.METRIC,
      avoidHighways: false,
      avoidTolls: false
    }, function (response, status) {
      if (status !== google.maps.DistanceMatrixStatus.OK) {
        alert('Error was: ' + status);
      } else {
        var originList = response.originAddresses;
        var destinationList = response.destinationAddresses;
        var element = response.rows[0].elements[0];
        var distance = element.distance.text;
        var duration = element.duration.text;
        $scope.$apply(function () {
          $scope.requestDistance = distance;
          $scope.requestDuration=duration;
        });


      }
    });
  }

});


app.controller('LoginCtrl', function ($scope, DriverLoginService, $location, $window) {
	if ($window.sessionStorage.token == "true") {$location.path('/drivers');}
	$scope.submit = function() {
		DriverLoginService.save({username: $scope.username, password: $scope.password}, function (response) {
			if (response.isLogin) {
		 		$scope.loginMessage = "logedIn";
				$window.sessionStorage.logged = true;
		 		$window.sessionStorage.token = true;
		 		$window.sessionStorage.userId = response.userId;
		 		$location.path('/drivers');
			} else {
				$window.sessionstorage.token = false;
				$window.sessionStorage.userId = false;
		 		$scope.loginMessage = "error";
				$window.sessionStorage.logged = false;
			}
		});
	};
});

app.controller('InvoicesCtrl', function ($scope, InvoicesService,InvoicePaymentService, $location, $window) {
	$scope.total='';
	$scope.bookings='';
	$scope.charged='';
  $scope.$parent.logged = $window.sessionStorage.logged;
  $scope.yesPayment=false;
  $scope.syncNotification='';
	if ($window.sessionStorage.token == "true") {
		InvoicesService.save({id: $window.sessionStorage.userId}, function (response) {
			$scope.total=response.total;
			$scope.bookings=response.bookings;
			$scope.charged=response.charged;
      $scope.yesPayment=response.yesPayment;
      $scope.syncNotification='';
    });

    $scope.pay=function() {
      InvoicePaymentService.save({id: $window.sessionStorage.userId}, function (response) {
        $scope.paid = response.paid;

        $scope.total='';
        $scope.bookings='';
        $scope.charged='';
        $scope.yesPayment=false;
        $scope.syncNotification='Thanks for payment';
      });
    }
	};
});

app.controller('LogoutCtrl', function ($scope, $location, $window) {
	if ($window.sessionStorage.token == "true") {
		$window.sessionStorage.token = false;
		$window.sessionStorage.userId = false;
		$window.sessionStorage.logged = false;
		$scope.$parent.logged = false;
		$location.path('/login');
	};
});

app.controller('StatusCtrl', function ($scope, StatusRequestService, StatusUpdateService, $window, $location) {
  $scope.asyncNotification = '';
  $scope.syncNotification = '';
  $scope.bookingid='';
  $scope.$parent.logged = $window.sessionStorage.logged;
  $scope.driverStatus='';
  $scope.disabled=false;

  $scope.statusList = [
    'Available',
    'Off-duty'
  ];

  if ($window.sessionStorage.token == "true") {
    StatusRequestService.save({id: $window.sessionStorage.userId}, function (response) {
      $scope.driverStatus = response.driverStatus;
      if(response.driverStatus=='Busy') {
        $scope.disabled = true;
      }
    });
  } else {
    $location.path('/login');
  }

  $scope.update = function(status) {
    StatusUpdateService.save({id: $window.sessionStorage.userId, driverStatus: status}, function(response){
      $scope.driverStatus = status;
      $window.location.reload();
    });
  };
});
