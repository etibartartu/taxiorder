var app = angular.module('driver_client', ['ngRoute','ngResource']);

app.constant('HEROKU_APP','https://tranquil-sierra-2170.herokuapp.com/');
//app.constant('HEROKU_APP','http://localhost:3000/');

app.run(function (DriversLocation,Framework,$window,$interval) { //use run rather than config
	//console.log($window,sessionStorage.token);

		this.findLocation=function(){
			Framework.navigator().then(function (navigator) {
			navigator.geolocation.getCurrentPosition(function(position) {
				//console.log("I am inside");
				var latitude = position.coords.latitude;
				var longitude = position.coords.longitude;
				//console.log(latitude);
				//console.log('log here'+ longitude);
				DriversLocation.save({lat: latitude, long: longitude, driverId:$window.sessionStorage.userId}, function (response) {});

			});

		});

		}

		$interval(function()
		{
			if ($window.sessionStorage.token == "true") {
				this.findLocation();
			}
			//console.log("inside");
		}.bind(this), 30000);
		//invoke initialy
		if ($window.sessionStorage.token == "true") {
			this.findLocation();
		}
		//console.log("inside");
})

app.config(function($routeProvider){
    $routeProvider
    	.when('/drivers', {
		  templateUrl: 'views/drivers/new.html',
		  controller: 'DriversCtrl'
		}).when('/login', {
		  templateUrl: 'views/drivers/login.html',
		  controller: 'LoginCtrl'
		})
		.when('/logout', {
			templateUrl: 'views/drivers/login.html',
			controller: 'LogoutCtrl'
		})
    .when('/drivers/requests', {
        templateUrl: 'views/drivers/request.html',
        controller: 'RequestsCtrl'
    })
	.when('/drivers/invoices', {
		templateUrl: 'views/drivers/invoice.html',
		controller: 'InvoicesCtrl'
	})
    .when('/drivers/status', {
      templateUrl: 'views/drivers/status.html',
      controller: 'StatusCtrl'
    })
    .otherwise({
        redirectTo: '/login'
    });
});

