'use strict';

var app = angular.module('driver_client');

app.service('DriversService', function ($resource,HEROKU_APP) {
  return $resource(HEROKU_APP+'drivers/accepted', {});
});

app.factory('DriversLocation' ,function($resource,HEROKU_APP){
  return $resource(HEROKU_APP+'drivers/location', {});
});

app.service('RequestsService', function ($resource,HEROKU_APP) {
  return $resource(HEROKU_APP+'drivers/request', {});
});

app.service('StatusRequestService', function ($resource,HEROKU_APP) {
  return $resource(HEROKU_APP+'drivers/requestStatus', {});
});

app.service('StatusUpdateService', function ($resource,HEROKU_APP) {
  return $resource(HEROKU_APP+'drivers/updateStatus', {});
});

app.service('RequestsAnswerService', function ($resource,HEROKU_APP) {
  return $resource(HEROKU_APP+'drivers/answer', {});
});

app.service('InvoicesService', function ($resource,HEROKU_APP) {
  return $resource(HEROKU_APP+'drivers/invoice', {});
});


app.service('InvoicePaymentService', function ($resource,HEROKU_APP) {
  return $resource(HEROKU_APP+'drivers/invoicePayment', {});
});




app.service('PusherService', function ($rootScope) {
  var pusher = new Pusher('ae34ee28ffd3fe570dad');
  var channel = pusher.subscribe('drivers');
  return {
    onMessage: function (callback) {
      channel.bind('async_notification', function (data) {
        $rootScope.$apply(function () {
          callback(data);
        });
      });
    }
  };
});

app.service('DriverLoginService', function ($resource,HEROKU_APP) {
  //console.log(HEROKU_APP);
  return $resource(HEROKU_APP+'login', {});

});

app.service('Framework', function ($q) {
  var _navigator = $q.defer();
  var _cordova = $q.defer();
  //console.log('I am in');

  if (window.cordova === undefined) {
    _navigator.resolve(window.navigator);
    _cordova.resolve(false);
  } else {
    document.addEventListener('deviceready', function (evt) {
      _navigator.resolve(navigator);
      _cordova.resolve(true);
    });
  }

  //console.log('I am in');

  return {
    navigator: function() { return _navigator.promise; },
    cordova: function() { return _cordova.promise; }
  };
});
