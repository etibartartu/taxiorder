'use strict';

describe('BookingsCtrl', function () {

  beforeEach(module('taksi_client'));

  var BookingsCtrl,
      BookingsService,
      window,
      rootScope,
      scope;

  beforeEach(inject(function ($controller, $rootScope, _BookingsService_, _$window_) {
    scope = $rootScope.$new();
    rootScope = $rootScope;
    window = _$window_;
    BookingsService = _BookingsService_;
    BookingsCtrl = $controller('BookingsCtrl', {
      $window: window,
      $scope: scope
    });
  }));

  it('should be connected to its view', function () {
    expect(scope.latitude).toBeDefined();
    expect(scope.longitude).toBeDefined();
    expect(scope.submit).toBeDefined();
  })

  var $httpBackend = {};

  beforeEach(inject(function (_$httpBackend_) {
    $httpBackend = _$httpBackend_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  beforeEach(function(){
    window.sessionStorage.bookingId = null;
    window.sessionStorage.token = null;
  });

  it('should submit a request to the backend service and return a message', function () {
    expect(rootScope.syncNotification).toBeUndefined();
    $httpBackend.expectPOST('http://localhost:3000/bookings', {
      latitude: '58.36224196380309',
      longitude: '26.696424047143523',
      destination: 'Võru 55f, 50410 Tartu, Estonia',
      fee: '4.45'
    }).respond(201, {
      message: 'Booking is being processed. Estimated fee: 4.45',
      bookingId: 18
    });
    scope.latitude = '58.36224196380309';
    scope.longitude = '26.696424047143523';
    scope.fee = '4.45';
    scope.destination = 'Võru 55f, 50410 Tartu, Estonia';
    scope.submit();
    $httpBackend.flush();
    expect(rootScope.syncNotification).toEqual('Booking is being processed. Estimated fee: 4.45');
    expect(window.sessionStorage.bookingId).toEqual('18');
    expect(window.sessionStorage.token).toEqual('true');
  });
});

describe('BookingsViewCtrl', function(){
  beforeEach(module('taksi_client'));

  var BookingsViewCtrl,
      scope,
      rootScope,
      BookingsCancelService,
      window

  var $httpBackend = {};

  beforeEach(inject(function (_$httpBackend_) {
    $httpBackend = _$httpBackend_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  beforeEach(inject(function($controller, $rootScope, _BookingsCancelService_, _$window_){
    scope = $rootScope.$new();
    rootScope = $rootScope;
    BookingsCancelService = _BookingsCancelService_;
    window = _$window_;
    //window.sessionStorage.bookingId = 18;
    BookingsViewCtrl = $controller('BookingsViewCtrl', {
      $window: window,
      $scope: scope
    });
  }));

  it('should test cancelation', function(){
    expect(rootScope.syncNotification).toBeUndefined();
    expect(rootScope.asyncNotification).toBeUndefined();
    $httpBackend.expectPOST('http://localhost:3000/bookings/cancel', {
      bookingId: '18'
    }).respond(201, {message: 'Booking is canceled.', bookingId: '18'});
    scope.cancel();
    $httpBackend.flush();
    expect(rootScope.syncNotification).toEqual('Booking is canceled.');
  });

  it('should test async', function(){
    expect(scope.asyncNotification).toEqual('');
    var stub = Pusher.instances[3];
    var channel = stub.channel('bookings');
    channel.emit('async_notification', {message: 'Car is coming'});
    expect(scope.asyncNotification).toEqual('Car is coming');
  });
});