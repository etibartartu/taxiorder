var app = angular.module('taksi_client', ['ngRoute','ngResource']);
app.constant('HEROKU_APP','https://tranquil-sierra-2170.herokuapp.com/');
//app.constant('HEROKU_APP','http://localhost:3000/');

app.config(function($routeProvider){
    $routeProvider
    	.when('/bookings', {
		  templateUrl: 'views/bookings/new.html',
		  controller: 'BookingsCtrl'
		})
        .when('/view', {
            templateUrl: 'views/bookings/view.html',
            controller: 'BookingsViewCtrl'
        })
        .otherwise({
            redirectTo: '/bookings'
        });
});

