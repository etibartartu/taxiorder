'use strict'

var app = angular.module('taksi_client');

app.controller('BookingsCtrl', function ($scope, BookingsService, PusherService,$window,$location,$rootScope, Framework) {
  $scope.latitude = 58.87313;
  $scope.longitude = 26.1453;
  $scope.source='';
  $scope.destination = '';
  $scope.distance="";
  $scope.duration="";
    $scope.fee=0;
  $scope.syncNotification = $rootScope.syncNotification;
  $scope.submit = function() {
    BookingsService.save(
        {
          latitude: $scope.latitude,
          longitude: $scope.longitude,
          destination: $scope.destination,
          fee: $scope.fee
        }, function (response) {
          $rootScope.syncNotification = response.message;
          $window.sessionStorage.token = true;
          $window.sessionStorage.bookingId = response.bookingId;
          $location.path('/view');
    });
  };
    Framework.navigator().then(function (navigator) {
        navigator.geolocation.getCurrentPosition(function (position) {
            console.log("I am inside");
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            var latto=position.coords.latitude+0.001;
            var lonto=position.coords.longitude+0.001;
            var distance = "";
            var duration = "";
            var destination='';
            var fee;
            var mapProp = {
                center: {lat: latitude, lng: longitude},
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scaleControl:true
            };




          $scope.map = new google.maps.Map(document.getElementById('map'), mapProp);
            var geocoder= new google.maps.Geocoder();
            $scope.geocode=function(towhich,lat,long){
                geocoder.geocode({'location':  {lat: parseFloat(lat), lng: parseFloat(long)}}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        destination=results[0]["formatted_address"];
                        console.log(towhich)
                        $scope.$apply(function () {
                          if(towhich==1){
                            $scope.source=destination;
                          }
                          else
                            $scope.destination=destination;
                        });
                    } else {
                        alert("Sorry, this search produced no results.");
                      return 0
                    }
                });
            }

          $scope.geocode(1,latitude,longitude);
          $scope.geocode(2,latto,lonto);


          var image = 'views/images/finish.png';
            var imagefrom = 'views/images/start.png';
            $scope.marker = new google.maps.Marker(
                {
                    map: $scope.map,
                    position: mapProp.center,
                    draggable: true,
                    icon:imagefrom

                });

            $scope.markerto = new google.maps.Marker(
                {
                    map: $scope.map,
                    position: {lat: latto, lng: lonto},
                    draggable: true,
                    icon: image
                });

            google.maps.event.addListener($scope.marker,'dragend',function(event) {

                latitude=event.latLng.lat();
                longitude=event.latLng.lng();
                $scope.geocode(1,latitude,longitude);
                $scope.applychanges();
                $scope.caldistance();
            });

            google.maps.event.addListener($scope.markerto,'dragend',function(event) {

                latto=event.latLng.lat();
                lonto=event.latLng.lng();
                $scope.geocode(2,latto,lonto);
                $scope.caldistance();
            });

            $scope.applychanges=function(){
                $scope.$apply(function () {
                    $scope.latitude = latitude;
                    $scope.longitude=longitude;
                });
            };

            var service = new google.maps.DistanceMatrixService
            $scope.caldistance= function()
            {
                service.getDistanceMatrix({
                    origins: [{lat: latitude, lng: longitude}],
                    destinations: [{lat: latto, lng: lonto}],
                    travelMode: google.maps.TravelMode.DRIVING,
                    unitSystem: google.maps.UnitSystem.METRIC,
                    avoidHighways: false,
                    avoidTolls: false
                }, function (response, status) {
                    if (status !== google.maps.DistanceMatrixStatus.OK) {
                        alert('Error was: ' + status);
                    } else {
                        var originList = response.originAddresses;
                        var destinationList = response.destinationAddresses;
                        var element = response.rows[0].elements[0];
                        var distance = element.distance.text;
                        var duration = element.duration.text;
                        var fee= (2.5+ parseFloat(distance)*0.75).toFixed(2);
                        console.log(fee)
                        $scope.$apply(function () {
                            $scope.fee = fee;
                            $scope.duration=duration;
                        });


                    }
                });
            }

            $scope.position=function geocodePosition(pos) {
                geocoder.geocode({
                    latLng: pos
                }, function(responses) {
                    if (responses && responses.length > 0) {
                        updateMarkerAddress(responses[0].formatted_address);
                    } else {
                        updateMarkerAddress('Cannot determine address at this location.');
                    }
                });
            }

            $scope.map.addListener($scope.marker, 'dragend', function() {
                console.log("update");
            });

            $scope.$apply(function () {
                $scope.latitude = latitude;
                $scope.longitude=longitude;
            });



            $scope.marker.addListener('click', function(){
                //$scope.marker.setAnimation(google.maps.Animation.BOUNCE);
                console.log("yes");
                console.log($scope.marker.getPosition().lat());
                console.log($scope.marker.getPosition().lng());
                //latitude=
                //longitude=$scope.marker.getPosition().lng();
                $scope.$apply(function () {
                    $scope.latitude = $scope.marker.getPosition().lat();
                    $scope.longitude=$scope.marker.getPosition().lng();
                });
            });
        });
    });





});


app.controller('BookingsViewCtrl', function ($scope, BookingsCancelService, PusherService,$window,$location,$rootScope) {
      console.log($rootScope.syncNotification);
      console.log($scope.syncNotification);

        $scope.asyncNotification = '';
        $scope.noAvailable=false;
        $scope.delivered=false;
        $scope.back=function(){
          $location.path('/bookings');
        }

        $scope.cancel = function() {
            console.log("here")
            BookingsCancelService.save(
        {
           bookingId:$window.sessionStorage.bookingId
        },
                function (response) {

                    $rootScope.syncNotification = response.message;
                    console.log("should go to bookings")
                    $location.path('/bookings');
        })
    };
      PusherService.onMessage(function(response) {
        if($window.sessionStorage.bookingId==response.bookingId && response.delivered) {
          $scope.delivered=response.delivered;

          console.log("delivered")
          console.log($scope.delivered);
        }
        else if($window.sessionStorage.bookingId==response.bookingId && response.noAvailable){
          $scope.noAvailable=response.noAvailable;
          console.log("no avaliable")
          console.log($scope.noAvailable);
        }
        else if($window.sessionStorage.bookingId==response.bookingId) {
          $scope.asyncNotification = response.message;
          $scope.delivered=response.delivered;
          $scope.pusherTrue = response.pusherTrue;
        }

      });
    }
);
