'use strict'

var app = angular.module('driver_client');

app.controller('DriversCtrl', function ($scope, DriversService, PusherService, DriverLoginService,RequestsAnswerService, $window, $location, $timeout) {
  $scope.asyncNotification = '';
	$scope.syncNotification = '';
	$scope.haveBooking=false;
	$scope.bookingid='';
	$scope.$parent.logged = $window.sessionStorage.logged;
	$scope.beginning='';
	$scope.destination='';
	$scope.fee=0;
	$scope.$parent.logged = $window.sessionStorage.logged;
	PusherService.onMessage(function(response) {
		$scope.asyncNotification = response.message;
		console.log(response.bookingId)
		$timeout( function(){ $scope.callAtTimeout(response.bookingId); }, 40000);
	});

	if ($window.sessionStorage.token == "true") {
	  DriversService.save({id: $window.sessionStorage.userId}, function (response) {
		  $scope.haveBooking=response.haveBooking;
		  $scope.syncNotification = response.message;
		  $scope.beginning=response.beginning;
		  $scope.destination=response.destination;
		  $scope.bookingid=response.bookingid;
		  $scope.fee=response.fee;
	  });
  } else {
		$location.path('/login');
  }

	$scope.callAtTimeout=function(bookingId){
		console.log("Timeout occurred");
		RequestsAnswerService.save({answer: 'ignored', bookingid: bookingId}, function (response) {
			$scope.syncNotification = response.message;
		});
	};


	$scope.submit = function() {
		RequestsAnswerService.save({answer: 'finished', bookingid: $scope.bookingid}, function (response) {
			$scope.syncNotification = response.message;
			$scope.haveBooking=false;
		});
	};
});

app.controller('LoginCtrl', function ($scope, DriverLoginService, $location, $window) {
	if ($window.sessionStorage.token == "true") {$location.path('/drivers');}
	$scope.submit = function() {
		DriverLoginService.save({username: $scope.username, password: $scope.password}, function (response) {
			if (response.isLogin) {
		 		$scope.loginMassage = "logedIn";
				$window.sessionStorage.logged = true;
		 		$window.sessionStorage.token = true;
		 		$window.sessionStorage.userId = response.userId;
		 		$location.path('/drivers');
			} else {
				$window.sessionstorage.token = false;
				$window.sessionStorage.userId = false;
		 		$scope.loginMassage = "error";
				$window.sessionStorage.logged = false;
			}
		});
	};
});

app.controller('InvoicesCtrl', function ($scope, InvoicesService, $location, $window) {
	$scope.total='';
	$scope.bookings='';
	$scope.charged='';
	if ($window.sessionStorage.token == "true") {
		InvoicesService.save({id: $window.sessionStorage.userId}, function (response) {
			$scope.total=response.total;
			$scope.bookings=response.bookings;
			$scope.charged=response.charged;

		});
	};
});

app.controller('LogoutCtrl', function ($scope, $location, $window) {
	if ($window.sessionStorage.token == "true") {
		$window.sessionStorage.token = false;
		$window.sessionStorage.userId = false;
		$window.sessionStorage.logged = false;
		$scope.$parent.logged = false;
		$location.path('/login');
	};
});