'use strict';

describe('DriversCtrl', function(){
  beforeEach(module('driver_client'));

  var DriverLoginService,
      DriversService,
      DriversCtrl,
      scope,
      window

  beforeEach(inject(function($controller, $rootScope, _$window_, _DriverLoginService_, _DriversService_){
    DriverLoginService = _DriverLoginService_;
    DriversService = _DriversService_;
    scope = $rootScope.$new();
    window = _$window_;
    window.sessionStorage.token = true;
    DriversCtrl = $controller('DriversCtrl', {
      $scope: scope,
      $window: window
    });
  }));

  it('should test something', function(){
    expect(scope.asyncNotification).toEqual('');
    expect(window.sessionStorage.token).toEqual('true');
    var stub = Pusher.instances[0];
    var channel = stub.channel('drivers');
    channel.emit('async_notification', {message: 'Logged in'});
    expect(scope.asyncNotification).toEqual('Logged in');
  });
});

describe('LoginCtrl', function(){
  beforeEach(module('driver_client'));

  var LoginCtrl,
      DriverLoginService,
      scope,
      window

  beforeEach(inject(function($controller, $rootScope, _$window_, _DriverLoginService_){
    scope = $rootScope.$new();
    window = _$window_;
    DriverLoginService = _DriverLoginService_;
    LoginCtrl = $controller('LoginCtrl', {
      $scope: scope,
      $window: window
    })
  }));

  var $httpBackend = {};

  beforeEach(inject(function (_$httpBackend_) {
    $httpBackend = _$httpBackend_;
  }));

  beforeEach(function(){
    window.sessionStorage.logged = false;
    window.sessionStorage.token = false;
    window.sessionStorage.userId = null;
    expect(scope.loginMassage).toBeUndefined();
  });

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should log in when correct username and password are submitted', function(){
    $httpBackend.expectPOST('http://localhost:3000/login', {username: 'username1', password: '1'})
        .respond(201, {message: 'Logged in', isLogin: true, userId: 13});
    scope.username = 'username1';
    scope.password = '1';
    scope.submit();
    $httpBackend.flush();
    expect(scope.loginMassage).toEqual('logedIn');
    expect(window.sessionStorage.logged).toEqual('true');
    expect(window.sessionStorage.token).toEqual('true');
    expect(window.sessionStorage.userId).toEqual('13');
  });

  it('should not log in if username/password incorrect', function(){
    $httpBackend.expectPOST('http://localhost:3000/login', {username: 'x', password: 'y'})
        .respond(500, {message: 'Error', isLogin: false, userId: null});
    scope.username = 'x';
    scope.password = 'y';
    scope.submit();
    $httpBackend.flush();
    expect(scope.loginMassage).toBeUndefined();
    expect(window.sessionStorage.logged).toEqual('false');
    expect(window.sessionStorage.token).toEqual('false');
    expect(window.sessionStorage.userId).toEqual('null');
  });
});

describe('Logout controller test', function(){
  beforeEach(module('driver_client'));

  var LogoutCtrl,
      scope,
      window

  beforeEach(inject(function($controller, $rootScope, _$window_){
    scope = $rootScope.$new();
    window = _$window_;
    window.sessionStorage.token = true;
    window.sessionStorage.userId = 'username1';
    window.sessionStorage.logged = true;
    scope.$parent.logged = true;
    LogoutCtrl = $controller('LogoutCtrl', {
      $scope: scope,
      $window: window,
    })
  }));

  //When Logout controller is reached it should set some session variables to false
  it('should logout if you are logged in', function(){
    expect(window.sessionStorage.token).toEqual('false');
    expect(window.sessionStorage.userId).toEqual('false');
    expect(window.sessionStorage.logged).toEqual('false');
    expect(scope.$parent.logged).toEqual(false);
  });
});