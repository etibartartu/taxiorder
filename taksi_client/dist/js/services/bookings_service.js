'use strict';

var app = angular.module('taksi_client');

app.service('BookingsService', function ($resource,HEROKU_APP) {
  console.log(HEROKU_APP);
  return $resource(HEROKU_APP+'bookings', {});
});

app.service('BookingsCancelService', function ($resource,HEROKU_APP) {
  console.log(HEROKU_APP);
  return $resource(HEROKU_APP+'bookings/cancel',{});
});

app.service('PusherService', function ($rootScope) {
  var pusher = new Pusher('ae34ee28ffd3fe570dad');
  var channel = pusher.subscribe('bookings');

  return {
    onMessage: function (callback) {
      channel.bind('async_notification', function (data) {
        $rootScope.$apply(function () {
          callback(data);
        });
      });
    }
  };
});

app.service('Framework', function ($q) {
  var _navigator = $q.defer();
  var _cordova = $q.defer();
  console.log('I am in');

  if (window.cordova === undefined) {
    _navigator.resolve(window.navigator);
    _cordova.resolve(false);
  } else {
    document.addEventListener('deviceready', function (evt) {
      _navigator.resolve(navigator);
      _cordova.resolve(true);
    });
  }

  console.log('I am in');

  return {
    navigator: function() { return _navigator.promise; },
    cordova: function() { return _cordova.promise; }
  };
});